import logging
import pprint
import base64
from io import BytesIO
from datetime import datetime
import tempfile
import json

import werkzeug
from odoo import http
from odoo.http import request
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.translate import _


_logger = logging.getLogger(__name__)

Error1 = _(
    "QRpagos Errors 1: cardconnect Payment Gateway Currently not Configure for this Currency pls Connect Your Shop Provider !!!")
Error2 = _("QRpagos Errors 2: Authentication Error: API keys are incorrect.")
Error3 = _("QRPAGOS Errors 3: Authorization Error: not authorized to perform the attempted action.")
Error4 = _("QRpagos Errors 4: Issue occure while generating clinet token, pls contact your shop provider.")
Error5 = _("QRpagos Errors 5: Default 'Merchant Account ID' not found.")
Error6 = _("QRpagos Errors 6: Transaction not Found.")
Error7 = _("QRpagos Errors 7: Error occured while payment processing or Some required data missing.")
Error8 = _("QRpagos Errors 8: Validation error occured. Please contact your administrator.")
Error9 = _(
    "QRpagos Errors 9: Payment has been recevied on cardconnect end but some error occured during processing the order.")
Error10 = _("QRpagos Errors 10: Unknow Error occured. Unable to validate the cardconnect payment.")
SuccessMsg = _("Payment Successfully recieved and submitted for settlement.")


class QrPagosController(http.Controller):

    @http.route(["/qrpagos/client_token"], type='json', auth="public", website=True)
    def client_token(self):
        try:
            return {'status': True, 'token': "74892365423647392"}
        except Exception as e:
            _logger.error("************Cardconnect exception occured 'client_token' ------- exception=%r", e)  # debug
            return {'status': False, 'message': Error4}

    def get_transaction_vals(self, form_vals, order=None):
        transaction_vals_dict = {}
        nonce_from_the_client = form_vals.get('payment_method_nonce')
        merchant_account_id = self.get_merchant_account_id()
        if nonce_from_the_client and merchant_account_id['status']:
            customer_obj = order.partner_id
            partner_billing_obj = order.partner_invoice_id
            partner_shipping_obj = order.partner_shipping_id
            transaction_vals_dict = {
                "amount": form_vals.get('amount'),
                "order_id": order.name,
                "payment_method_nonce": nonce_from_the_client,
                "merchant_account_id": merchant_account_id['merchant_account_id'],
                "options": {
                    "submit_for_settlement": True
                },
                "customer": {
                    "first_name": str(customer_obj.name),
                    "company": str(customer_obj.street),
                    "email": str(customer_obj.email),
                    "phone": str(customer_obj.phone),
                },
                "billing": {
                    "first_name": str(partner_billing_obj.name),
                    "company": str(partner_billing_obj.street),
                    "street_address": str(partner_billing_obj.street2),
                    "locality": str(partner_billing_obj.city),
                    "region": str(partner_billing_obj.state_id.name),
                    "postal_code": str(partner_billing_obj.zip),
                    "country_name": str(partner_billing_obj.country_id.name),
                    "country_code_alpha2": str(partner_billing_obj.country_id.code)
                },
                "shipping": {
                    "first_name": str(partner_shipping_obj.name),
                    "street_address": str(partner_shipping_obj.street),
                    "locality": str(partner_shipping_obj.city),
                    "region": str(partner_shipping_obj.state_id.name),
                    "postal_code": str(partner_shipping_obj.zip),
                    "country_name": str(partner_shipping_obj.country_id.name),
                    "country_code_alpha2": str(partner_shipping_obj.country_id.code)
                }
            }
        return transaction_vals_dict

    def qrpagos_do_payment(self, **post):
        #kobtner la orden , referencia 
        order, reference, tx = request.website.sale_get_order(), post.get('reference'), None
        values = {
            'status':   True,
            'message': '',
            'redirect_brt': False,  # True value redirect on QrPago error custom page
        }
        _logger.info('Primer Filtro', pprint.pformat(values)) 
        try:
            #obener el aqquirer id
            

            PaymentAcquirer = request.env['payment.acquirer']
            acquirere_id = \
            request.env['ir.model.data'].get_object_reference('odoo_pagos_qr', 'payment_acquirer_qrpagos')[1]
            _logger.info('aqquirer', pprint.pformat(acquirere_id)) 
            acquirer_credential = PaymentAcquirer.sudo().browse(acquirere_id)
            environment = acquirer_credential.environment
            #merchant_id = acquirer_credential.qrpagos_merchant_account
            customer_obj = order.partner_id

            aq = request.env['payment.acquirer'].sudo().browse(int(post.get('qrpagos-acquirer-id')))
          
            if reference:
                tx = request.env['payment.transaction'].sudo().search([('reference', '=', reference)])
            if tx:
                _logger.info('QRPagos form Corectamente with values %s', pprint.pformat(values))
                
                tx.sudo().write({
                    #'brt_txnid': values['acquirer_reference'],
                    #'acquirer_reference': values['acquirer_reference'],
                    'state': str(aq.estado_done),
                    #'state':'authorized', este estado esta observado
                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'state_message': SuccessMsg,
                    'captura_pago'  : base64.b64encode(post.get('captura').read()),
                    'app_qr': post.get('app_qr')
                })
                values.update({'status': True}) 
               
        except Exception as e:
            _logger.error(
                "************QrPagos exception occured ******************** \n 'qrpagos_payment' ------- exception=%r",
                e)  # debug
            if reference:
                tx = request.env['payment.transaction'].search([('reference', '=', reference)])

            if tx and values['status']:
                tx.sudo().write({
                    #'brt_txnid': values['acquirer_reference'],
                    #'acquirer_reference': values['acquirer_reference'],
                    'state': str(aq.estado_error),
                    'date': datetime.now().strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                    'state_message': Error9,
                })
                values.update({'status': False, 'redirect_brt': True, 'message': Error9})
            elif tx and not values['status']:
                tx.sudo()._set_transaction_error(Error1)
                values.update({'status': False, 'redirect_brt': True, 'message': e or Error1})
            elif not tx:
                values.update({'status': False, 'redirect_brt': True, 'message': Error6})
            else:
                values.update({'status': False, 'redirect_brt': True, 'message': Error10})
        return values

    @http.route('/payment/qrpagos', type='http', auth="public", website=True)
    def qrpagos_payment(self, **post):
        """ Qr Pagos Payment Controller """
        
        result = self.qrpagos_do_payment(**post)

        if not result['status']:
            _logger.error(result['message'])
        #return werkzeug.utils.redirect('/payment/process')
        return werkzeug.utils.redirect('/shop/confirmation')

    @http.route(['/qrpagos/modal'], type='json', auth="public", methods=['POST'], website=True)
    def qrpagos_modal(self):
        order         = request.website.sale_get_order()
        acquirere_id  = request.website.get_qrpagos_payment_acquirere_id()
        acquirere     = request.env['payment.acquirer'].sudo().browse(int(acquirere_id))
        payments_qr = acquirere.apps_qr
        #payments_qr    = request.env['payment.appsqr'].search([('qrpagos', '=', acquirere_id ),('activo','=',True)])
        vals = {
            'return_url': '/shop/payment/validate',
            'reference': '/',
            'amount': order.amount_total,
            'currency': order.currency_id,
            'qrpagos-acquirer-id': acquirere_id,
            'payments_qr': payments_qr,
            'pais' : order.partner_id.country_id
        }
        _logger.info('ajaaa')  # debug
        return request.env['ir.ui.view'].render_template("odoo_pagos_qr.qrpagos_template_modal", vals)

    @http.route(['/qrpagos/qrapps'], type='json', auth="public", methods=['POST'], website=True)
    def qrpagos_apps(self):
        order         = request.website.sale_get_order()
        acquirere_id  = request.website.get_qrpagos_payment_acquirere_id()
        acquirere     = request.env['payment.acquirer'].sudo().browse(int(acquirere_id))
        payments_qr = acquirere.apps_qr
        #payments_qr    = request.env['payment.appsqr'].search([('qrpagos', '=', acquirere_id ),('activo','=',True)])
        _logger.info('Datos Obtenidos%s', pprint.pformat(payments_qr))  # debug
        vals = {
              'aplicaciones':payments_qr,
              'pais' : order.partner_id.country_id
        }

        return request.env['ir.ui.view'].render_template("odoo_pagos_qr.qrpagos_template_apps", vals)



        
