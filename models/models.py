# -*- coding: utf-8 -*-
import logging

from odoo import api, fields, models, _
from odoo.addons.payment.models.payment_acquirer import ValidationError
from odoo.tools.float_utils import float_compare

_logger = logging.getLogger(__name__)

#"""
class AppsQr(models.Model):
    _name           = 'payment.appsqr'
    name            =  fields.Char(required=True)
    icono           =  fields.Binary(string="icon")
    qr              =  fields.Binary(string='upload QR')
    cant_max        =  fields.Float(string='maximum pay')
    activo          =  fields.Boolean(string="active",default=False)
    number_phone    =  fields.Char()
    paises          =  fields.Many2many('res.country')
    rpaises         =  fields.Char(string="paises",compute="paises_r")   
    def toggle_activo(self):
        for x in self:
            if x.qr:
                if x.activo == True:
                    x.activo = False
                else:
                    x.activo = True
                
            else:
                if x.activo == False:
                    raise Warning(_('upload QR'))
                else:
                    x.activo = False

    def paises_r(self):
        for x in self:
            c = 0
            for y in x.paises:
                if(c<4):
                    if (x.rpaises==False):
                        v = ' '
                    else:
                        v = x.rpaises
                    if c == 3:
                        x.rpaises = str(v)+ ' '+y.name+' ...'
                    else:
                        x.rpaises = str(v)+' '+y.name
                c += 1
           
class QrPagos(models.Model):
    _inherit     = 'payment.acquirer'
    provider     = fields.Selection(selection_add=[('qrpagos', 'QrPagos')])
    apps_qr      = fields.Many2many('payment.appsqr')
    estado_done = fields.Selection([('pending','Pending'),('done','done')],default='pending')
    estado_error  = fields.Selection([('cancel','Cancel'),('error','Error')],default='error')

    @api.multi
    def qrpagos_form_generate_values(self, tx_values):
        self.ensure_one()
        qrpagos_tx_values = dict(tx_values)
        temp_qrpagos_tx_values = {
            'company': self.company_id.name,
            'amount': tx_values.get('amount'),
            'currency': tx_values.get('currency') and tx_values.get('currency') or '',
            'currency_id': tx_values.get('currency') and tx_values.get('currency').id or '',
            'address_line1': tx_values['partner_address'],
            'address_city': tx_values['partner_city'],
            'address_country': tx_values['partner_country'] and tx_values['partner_country'].name or '',
            'email': tx_values['partner_email'],
            'address_zip': tx_values['partner_zip'],
            'name': tx_values['partner_name'],
            'phone': tx_values['partner_phone'],
        }

        temp_qrpagos_tx_values['returndata'] = qrpagos_tx_values.pop('return_url', '')
        qrpagos_tx_values.update(temp_qrpagos_tx_values)
        return qrpagos_tx_values
    @api.model
    def _get_qrpagos_urls(self, environment):
        return {
            'qrpagos_main_url': '/payment/qrpagos',
        }
    def qrpagos_get_form_action_url(self):
        return self._get_qrpagos_urls(self.environment)['qrpagos_main_url']


class TransactionQrPagos(models.Model):
    _inherit = 'payment.transaction'

    captura_pago = fields.Binary('payment photo')
    app_qr       = fields.Many2one('payment.appsqr' , string="application")

    @api.model
    def _qrpagos_form_get_tx_from_data(self, data):
        _logger.info("********************form data=%r", data)
        reference, amount, currency, acquirer_reference = data.get('reference'), data.get('amount'), data.get(
            'currency'), data.get('acquirer_reference')

        if not reference or not amount or not currency or not acquirer_reference:
            error_msg = 'QrPagos: received data with missing reference (%s) or acquirer_reference (%s) or Amount (%s)' % (
                reference, acquirer_reference, amount)
            _logger.error(error_msg)
            raise ValidationError(error_msg)

        tx = self.search([('reference', '=', reference)])
        if not tx or len(tx) > 1:
            error_msg = _('received data for reference %s') % (pprint.pformat(reference))
            if not tx:
                error_msg += _('; no order found')
            else:
                error_msg += _('; multiple order found')
            _logger.info(error_msg)
            raise ValidationError(error_msg)

        return tx

    @api.multi
    def _qrpagos_form_get_invalid_parameters(self, data):
        invalid_parameters = []
        if float_compare(float(data.get('amount', '0.0')), self.amount, 2) != 0:
            invalid_parameters.append(('amount', data.get('amount'), '%.2f' % self.amount))
        if data.get('currency') != self.currency_id.name:
            invalid_parameters.append(('currency', data.get('currency'), self.currency_id.name))
        return invalid_parameters

    @api.multi
    def _qrpagos_form_validate(self, data):
        status = data.get('status')
        res = {
            #'brt_txnid': data.get('acquirer_reference'),
            'acquirer_reference': data.get('acquirer_reference'),
            'state_message': data.get('tx_msg'),
            #'brt_txcurrency': data.get('currency'),
        }
        if status:
            _logger.info('Validated QrPagos payment for tx %s: set as done' % (self.reference))
            self._set_transaction_done()
            return self.write(res)

class WebsiteQrPagos(models.Model):
    _inherit = 'website'
    
    @api.model
    def get_qrpagos_payment_acquirere_id(self):
        IrModelData = self.env['ir.model.data']
        print(IrModelData.sudo().get_object_reference('odoo_pagos_qr', 'payment_acquirer_qrpagos'))
        acquirere_id = IrModelData.sudo().get_object_reference('odoo_pagos_qr', 'payment_acquirer_qrpagos')[1]
        return acquirere_id if acquirere_id else 0
    @api.model
    def get_data_total(self , partner):
        return partner



#"""

    