# Translation of Odoo Server.
# This file contains the translation of the following modules:
#	* odoo_pagos_qr
#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Server 12.0-20200613\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-07-25 15:57+0000\n"
"PO-Revision-Date: 2020-07-25 15:57+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
msgid "&amp;times;"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:12
#, python-format
msgid "&times;"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:64
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid ", then do the payment"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/models/models.py:111
#, python-format
msgid "; multiple order found"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/models/models.py:109
#, python-format
msgid "; no order found"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:payment.acquirer,cancel_msg:odoo_pagos_qr.payment_acquirer_qrpagos
msgid "<span><i>Cancelar,</i> Su pago ha sido cancelado.</span>"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:payment.acquirer,error_msg:odoo_pagos_qr.payment_acquirer_qrpagos
msgid "<span><i>Error,</i> Tenga en cuenta que se produjo un error durante la transacción. El pedido ha sido confirmado pero no será pagado. No dude en contactarnos si tiene alguna pregunta sobre el estado de su pedido.</span>"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:payment.acquirer,done_msg:odoo_pagos_qr.payment_acquirer_qrpagos
msgid "<span><i>Hecho,</i>Su pago en línea ha sido procesado con éxito. Gracias por su orden.</span>"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:payment.acquirer,pending_msg:odoo_pagos_qr.payment_acquirer_qrpagos
msgid "<span><i>Pendiente,</i>Su pago en línea ha sido procesado con éxito. Pero su pedido no está validado aún.</span>"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:63
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "Amount to pay :"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.acquirer_form_qrpagos
msgid "Apps QR"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.actions.act_window,name:odoo_pagos_qr.apps_qr_action
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_acquirer__apps_qr
#: model:ir.ui.menu,name:odoo_pagos_qr.appsqr_menu
msgid "Apps Qr"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.payapps_tree_view
msgid "Apps Tree"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:83
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "Attach the screenshot of the payment receipt"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,estado_error:0
msgid "Cancel"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:180
#, python-format
msgid "Cannot set-up the payment"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_checkout_s2s_form
msgid "Chekout para el formulario"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:49
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "Copy Cell No."
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.actions.act_window,help:odoo_pagos_qr.apps_qr_action
msgid "Crea tu primera App QR"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.actions.act_window,help:odoo_pagos_qr.app_list_action
msgid "Create your APP QR"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__create_uid
msgid "Created by"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__create_date
msgid "Created on"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__display_name
msgid "Display Name"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,estado_error:0
msgid "Error"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:232
#, python-format
msgid "Error: "
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_acquirer__estado_done
msgid "Estado Done"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_acquirer__estado_error
msgid "Estado Error"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:93
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "Finalize"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__id
msgid "ID"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr____last_update
msgid "Last Modified on"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__write_uid
msgid "Last Updated by"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__write_date
msgid "Last Updated on"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,provider:0
msgid "Manual Configuration"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__name
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.Planv_form_view
msgid "Name"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:186
#, python-format
msgid "No payment method selected"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__number_phone
msgid "Number Phone"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:236
#, python-format
msgid "Ok"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__paises
msgid "Paises"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.actions.act_window,name:odoo_pagos_qr.app_list_action
msgid "Pay Apps"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model,name:odoo_pagos_qr.model_payment_acquirer
msgid "Payment Acquirer"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:30
#, python-format
msgid "Payment Successfully recieved and submitted for settlement."
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model,name:odoo_pagos_qr.model_payment_transaction
msgid "Payment Transaction"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,estado_done:0
msgid "Pending"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:187
#, python-format
msgid "Please select a payment method."
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_acquirer__provider
msgid "Provider"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.Planv_form_view
msgid "QR"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:21
#, python-format
msgid "QRPAGOS Errors 3: Authorization Error: not authorized to perform the attempted action."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:29
#, python-format
msgid "QRpagos Errors 10: Unknow Error occured. Unable to validate the cardconnect payment."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:18
#, python-format
msgid "QRpagos Errors 1: cardconnect Payment Gateway Currently not Configure for this Currency pls Connect Your Shop Provider !!!"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:20
#, python-format
msgid "QRpagos Errors 2: Authentication Error: API keys are incorrect."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:22
#, python-format
msgid "QRpagos Errors 4: Issue occure while generating clinet token, pls contact your shop provider."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:23
#, python-format
msgid "QRpagos Errors 5: Default 'Merchant Account ID' not found."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:24
#, python-format
msgid "QRpagos Errors 6: Transaction not Found."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:25
#, python-format
msgid "QRpagos Errors 7: Error occured while payment processing or Some required data missing."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:26
#, python-format
msgid "QRpagos Errors 8: Validation error occured. Please contact your administrator."
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/controllers/controllers.py:27
#, python-format
msgid "QRpagos Errors 9: Payment has been recevied on cardconnect end but some error occured during processing the order."
msgstr ""

#. module: odoo_pagos_qr
#: model:payment.acquirer,name:odoo_pagos_qr.payment_acquirer_qrpagos
#: selection:payment.acquirer,provider:0
msgid "QrPagos"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:64
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "Scan the QR code or add our cell phone number at"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:166
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:172
#, python-format
msgid "Server Error"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.Planv_form_view
msgid "Tarea Form"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:167
#, python-format
msgid "We are not able to redirect you to the payment form."
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:173
#, python-format
msgid "We are not able to redirect you to the payment form. "
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/js/qrpagos_form.js:181
#, python-format
msgid "We're unable to process your payment."
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model,name:odoo_pagos_qr.model_website
msgid "Website"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,provider:0
msgid "Wire Transfer"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__activo
msgid "active"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_transaction__app_qr
msgid "application"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:27
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "choose a QR payment application"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:67
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
#, python-format
msgid "continue"
msgstr ""

#. module: odoo_pagos_qr
#: selection:payment.acquirer,estado_done:0
msgid "done"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__icono
msgid "icon"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__cant_max
msgid "maximum pay"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__rpaises
msgid "paises"
msgstr ""

#. module: odoo_pagos_qr
#. openerp-web
#: code:addons/odoo_pagos_qr/static/src/xml/template.xml:11
#, python-format
msgid "pay with"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.qrpagos_template_modal
msgid "pay with<span class=\"eltitulo\"> </span>"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_transaction__captura_pago
msgid "payment photo"
msgstr ""

#. module: odoo_pagos_qr
#: model:ir.model,name:odoo_pagos_qr.model_payment_appsqr
msgid "payment.appsqr"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.transaction_form_qrpagos
msgid "proof of payment"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/models/models.py:107
#, python-format
msgid "received data for reference %s"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.Planv_form_view
msgid "setting"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.acquirer_form_qrpagos
msgid "status in case of error"
msgstr ""

#. module: odoo_pagos_qr
#: model_terms:ir.ui.view,arch_db:odoo_pagos_qr.acquirer_form_qrpagos
msgid "status of the process performed"
msgstr ""

#. module: odoo_pagos_qr
#: code:addons/odoo_pagos_qr/models/models.py:31
#: model:ir.model.fields,field_description:odoo_pagos_qr.field_payment_appsqr__qr
#, python-format
msgid "upload QR"
msgstr ""