# -*- coding: utf-8 -*-
{
    'name': "Vex soluciones qr payments",

    'summary': """
       Module to pay with QR applications""",

    'description': """
      Module to pay with QR applications
    """,

    'author': "Jesus Sanchez Jibaja",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Payment',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base','payment',  'website_sale' ],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/qrpagos_views.xml',
        'views/payment_qrpagos.xml',
        'views/qrpagos_acquirer.xml',
        'data/payment_acquirer_qrpagos.xml',
        'data/payment_appsqr.xml',
        'static/src/xml/template.xml',
        'static/src/xml/apps.xml',
    ],
    'demo': [
    ],
    'images': ['static/src/img/logo_vex.png'],
    'qweb': [],
    'installable': True,
    'live_test_url': '',
    'auto_install': False,
    'application': False,
    "post_init_hook": "create_missing_journal_for_acquirers",
}